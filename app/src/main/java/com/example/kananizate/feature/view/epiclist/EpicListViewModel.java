package com.example.kananizate.feature.view.epiclist;

import static com.example.kananizate.feature.ConstantUtils.APP_NAME;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.ViewModel;

import com.example.kananizate.feature.data.EpicList;
import com.example.kananizate.feature.domain.usecase.EpicListUseCase;



public class EpicListViewModel extends ViewModel {
    private EpicListUseCase useCase;
    private EpicList list;
    private View progressBar, titleProgressBar, listLayout;

    public EpicListViewModel(EpicListUseCase useCase) {
        this.useCase = useCase;
    }

    public EpicList getEpicList(){
        progressBar.setVisibility(View.VISIBLE);
        titleProgressBar.setVisibility(View.VISIBLE);
        listLayout.setVisibility(View.GONE);
        list = new EpicList();
        useCase.start(new EpicListUseCase.EpicCallBack() {
            @Override
            public void onSuccess(EpicList epicList) {
                progressBar.setVisibility(View.GONE);
                titleProgressBar.setVisibility(View.GONE);
                listLayout.setVisibility(View.VISIBLE);
                try {
                    Thread.sleep(120);
                    list.setEpic(epicList.getEpic());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFail(Throwable error) {
                Log.i(APP_NAME, "Error: " + error.getMessage());
                progressBar.setVisibility(View.VISIBLE);
                titleProgressBar.setVisibility(View.VISIBLE);
                listLayout.setVisibility(View.GONE);
            }
        });
        return list;
    }

    public void setView(View progressBar, View titleProgressBar, View listLayout){
        this.progressBar = progressBar;
        this.titleProgressBar = titleProgressBar;
        this.listLayout = listLayout;
    }
}

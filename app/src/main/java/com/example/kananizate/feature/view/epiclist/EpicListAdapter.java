package com.example.kananizate.feature.view.epiclist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kananizate.R;
import com.example.kananizate.feature.data.EpicListItem;

import java.util.ArrayList;

public class EpicListAdapter extends RecyclerView.Adapter<EpicListHolder> {
    private ArrayList<EpicListItem> list;
    private Context context;
    private EpicListRecyclerListener listener;

    public EpicListAdapter(ArrayList<EpicListItem> list, Context context, EpicListRecyclerListener listener) {
        this.list = list;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public EpicListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_epiclist_item, parent, false);
        EpicListHolder holder = new EpicListHolder(view,listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull EpicListHolder holder, int position) {
        holder.setItemData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

package com.example.kananizate.feature.domain.repository;

import com.example.kananizate.feature.data.UserHistoryList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserHistoryListRepository {

    @GET("kanbanizate/item")
    Call<UserHistoryList> getUserHistoryList();
}

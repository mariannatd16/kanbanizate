package com.example.kananizate.feature.domain.usecase;

import static com.example.kananizate.feature.ConstantUtils.EPIC_USECASE_CLASS;
import static com.example.kananizate.feature.ConstantUtils.URL_BASE;

import android.util.Log;

import com.example.kananizate.feature.data.EpicList;
import com.example.kananizate.feature.data.EpicListItem;
import com.example.kananizate.feature.domain.repository.EpicListRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EpicListUseCase {

    public interface EpicCallBack{
        void onSuccess(EpicList list);
        void onFail(Throwable error);
    }

    public EpicListUseCase() { }

    public static void start(final EpicCallBack callBack){

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        EpicListRepository repository = retrofit.create(EpicListRepository.class);

        Call<EpicList> called = repository.getEpicList();

        called.enqueue(new Callback<EpicList>() {
            @Override
            public void onResponse(Call<EpicList> call, Response<EpicList> response) {
                if (response.isSuccessful()){

                    EpicList list = response.body();
                    for(EpicListItem item : list.getEpic()){
                        Log.w(EPIC_USECASE_CLASS, "Success: "+item.getId());
                    }
                    if(callBack != null){
                        callBack.onSuccess(list);
                    }
                }else{
                    Log.e(EPIC_USECASE_CLASS, "Error: " + response.code());
                    return;
                }
            }

            @Override
            public void onFailure(Call<EpicList> call, Throwable t) {
                Log.e(EPIC_USECASE_CLASS, "Error: "+t.getMessage());
                if(callBack != null){
                    callBack.onFail(t);
                }
                return;
            }
        });

    }
}

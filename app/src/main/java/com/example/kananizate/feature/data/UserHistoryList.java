package com.example.kananizate.feature.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class UserHistoryList implements Parcelable {
    private ArrayList<UserHistoryListItem> user_history;

    protected UserHistoryList(Parcel in) {
        user_history = in.createTypedArrayList(UserHistoryListItem.CREATOR);
    }

    public UserHistoryList() {
    }

    public static final Creator<UserHistoryList> CREATOR = new Creator<UserHistoryList>() {
        @Override
        public UserHistoryList createFromParcel(Parcel in) {
            return new UserHistoryList(in);
        }

        @Override
        public UserHistoryList[] newArray(int size) {
            return new UserHistoryList[size];
        }
    };

    public ArrayList<UserHistoryListItem> getUser_history() {
        return user_history;
    }

    public void setUser_history(ArrayList<UserHistoryListItem> user_history) {
        this.user_history = user_history;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(user_history);
    }
}

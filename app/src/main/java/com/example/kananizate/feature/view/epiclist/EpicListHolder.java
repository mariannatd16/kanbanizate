package com.example.kananizate.feature.view.epiclist;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kananizate.R;
import com.example.kananizate.feature.data.EpicListItem;

public class EpicListHolder extends RecyclerView.ViewHolder{
    private EpicListRecyclerListener listener;
    private EpicListItem item;

    TextView title, author, status;
    ImageView imageView;
    CardView layout;

    public EpicListHolder(@NonNull View itemView, EpicListRecyclerListener listener) {
        super(itemView);
        this.item = item;
        this.listener = listener;
        bindViews(itemView);

    }

    private void bindViews(View itemView) {
        title = itemView.findViewById(R.id.tvEpicTitle);
        author = itemView.findViewById(R.id.tvEpicAuthor);
        status = itemView.findViewById(R.id.tvEpicState);
        layout = itemView.findViewById(R.id.cardlayout);
    }

    public void setItemData( EpicListItem item){

        title.setText(item.getEpic_name());
        author.setText(item.getEpic_author());

        if(item.isIs_remote()){
            status.setText(R.string.kananizate_repository + " "+R.string.kabanizate_remoto);
        }else{
            status.setText(R.string.kananizate_repository + " "+R.string.kabanizate_local);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.isIs_remote()) {
                    status.setText(R.string.kananizate_repository + " "+R.string.kabanizate_local);
                    imageView.setImageResource(R.drawable.ic_cancel);
                }else{
                    status.setText(R.string.kananizate_repository + " "+R.string.kabanizate_remoto);
                    imageView.setImageResource(R.drawable.ic_mas);
                }
            }
        });

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo
            }
        });
    }
}

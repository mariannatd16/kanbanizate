package com.example.kananizate.feature.domain.repository;

import com.example.kananizate.feature.data.EpicList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EpicListRepository {

    @GET("kanbanizate/list")
    Call<EpicList> getEpicList();
}

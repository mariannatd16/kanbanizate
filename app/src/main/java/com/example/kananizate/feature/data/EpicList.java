package com.example.kananizate.feature.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class EpicList implements Parcelable {
    public ArrayList<EpicListItem> epic;

    public EpicList() {}

    protected EpicList(Parcel in) {
        epic = in.createTypedArrayList(EpicListItem.CREATOR);
    }

    public static final Creator<EpicList> CREATOR = new Creator<EpicList>() {
        @Override
        public EpicList createFromParcel(Parcel in) {
            return new EpicList(in);
        }

        @Override
        public EpicList[] newArray(int size) {
            return new EpicList[size];
        }
    };

    public ArrayList<EpicListItem> getEpic() {
        return epic;
    }

    public void setEpic(ArrayList<EpicListItem> epic) {
        this.epic = epic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(epic);
    }
}

package com.example.kananizate.feature;

public class ConstantUtils {

    public static String URL_BASE = "http://localhost:3001/";//http://localhost:8888/kanbanizate/list;
    public static String APP_NAME = "Kanbanizate";
    public static String EPIC_USECASE_CLASS = "EpicListUseCase";
    public static String USER_HISTORY_USECASE_CLASS = "UserHistoryListUseCase";
    public static String EPIC_LIST_FRAGMENT = "EpicListFragment";
    public static String USER_HISTORY_FRAGMENT = "UserHistoryListFragment";
}

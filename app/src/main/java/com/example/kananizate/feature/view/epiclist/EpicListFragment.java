package com.example.kananizate.feature.view.epiclist;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kananizate.R;
import com.example.kananizate.feature.data.EpicList;
import com.example.kananizate.feature.data.EpicListItem;
import com.example.kananizate.feature.domain.usecase.EpicListUseCase;

import java.util.ArrayList;


public class EpicListFragment extends Fragment
        implements EpicListRecyclerListener{

    private View rootLayout, separator;
    private ProgressBar progressBar;
    private TextView textViewProgress, textViewRemote, textViewLocal;
    private RecyclerView recyclerViewRemote, recyclerViewLocal;
    private ConstraintLayout listLayout;


    private EpicList epicList;
    private ArrayList<EpicListItem> remoteList, localList;

    private EpicListViewModel viewModel;
    private EpicListUseCase useCase;

    public static EpicListFragment newInstance(){
        EpicListFragment fragment = new EpicListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootLayout = inflater.inflate(R.layout.layout_epiclist_fragment, container, false);
        bindViews(rootLayout);
        setData();
        return rootLayout;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        viewModel = new EpicListViewModel(useCase);
        super.onAttach(context);
    }

    private void bindViews(View rootLayout){
        progressBar = rootLayout.findViewById(R.id.pgDownload);
        textViewProgress = rootLayout.findViewById(R.id.tvLoading);
        textViewRemote = rootLayout.findViewById(R.id.titleRemote);
        textViewLocal = rootLayout.findViewById(R.id.titleLocal);
        recyclerViewRemote = rootLayout.findViewById(R.id.rvEpicListRemote);
        recyclerViewLocal = rootLayout.findViewById(R.id.rvEpicListLocal);
        listLayout = rootLayout.findViewById(R.id.listContainer);
        separator = rootLayout.findViewById(R.id.localSeparator);
        viewModel.setView(progressBar, textViewProgress, listLayout);

    }

    private void setData(){
        epicList = new EpicList();
        epicList.setEpic(viewModel.getEpicList().getEpic());
        /*if (epicList != null || !epicList.getEpic().isEmpty()){

            Log.i(EPIC_LIST_FRAGMENT, "list:"+epicList.getEpic().get(0));
                setAdapter();
        }*/

    }

    private void setAdapter(){

        EpicListAdapter adapterRemote = new EpicListAdapter(remoteList, rootLayout.getContext(), this);
        EpicListAdapter adapterLocal = new EpicListAdapter(localList, rootLayout.getContext(), this);
        recyclerViewRemote.setAdapter(adapterRemote);
        recyclerViewLocal.setAdapter(adapterLocal);
    }

    @Override
    public void changeStatus() {
        for (EpicListItem item : epicList.getEpic()){
            if(item.isIs_remote()){
                localList.add(item);
                remoteList.remove(item);
            }else{
                remoteList.add(item);
                localList.remove(item);
            }
        }
    }
}

package com.example.kananizate.feature.view.epiclist;

import static com.example.kananizate.feature.ConstantUtils.EPIC_LIST_FRAGMENT;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.kananizate.R;
;

public class EpicListActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_epiclist_activity);
        bindView();
        setFragment();
    }

    private void bindView(){
        toolbar = findViewById(R.id.TopBar);
    }

    private void setFragment(){
        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction()
                .add(R.id.fgEpicList, EpicListFragment.newInstance(), EPIC_LIST_FRAGMENT);
        transaction.commit();
    }
}
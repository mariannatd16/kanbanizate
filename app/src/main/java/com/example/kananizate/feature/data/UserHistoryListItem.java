package com.example.kananizate.feature.data;

import android.os.Parcel;
import android.os.Parcelable;

public class UserHistoryListItem implements Parcelable {
    private int user_id,
                state;

    private String user_history_name,
                   user_history_desc;

    public UserHistoryListItem() { }

    protected UserHistoryListItem(Parcel in) {
        user_id = in.readInt();
        state = in.readInt();
        user_history_name = in.readString();
        user_history_desc = in.readString();
    }

    public static final Creator<UserHistoryListItem> CREATOR = new Creator<UserHistoryListItem>() {
        @Override
        public UserHistoryListItem createFromParcel(Parcel in) {
            return new UserHistoryListItem(in);
        }

        @Override
        public UserHistoryListItem[] newArray(int size) {
            return new UserHistoryListItem[size];
        }
    };

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getUser_history_name() {
        return user_history_name;
    }

    public void setUser_history_name(String user_history_name) {
        this.user_history_name = user_history_name;
    }

    public String getUser_history_desc() {
        return user_history_desc;
    }

    public void setUser_history_desc(String user_history_desc) {
        this.user_history_desc = user_history_desc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(user_id);
        parcel.writeInt(state);
        parcel.writeString(user_history_name);
        parcel.writeString(user_history_desc);
    }
}

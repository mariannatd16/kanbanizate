package com.example.kananizate.feature.domain.usecase;

import static com.example.kananizate.feature.ConstantUtils.URL_BASE;
import static com.example.kananizate.feature.ConstantUtils.USER_HISTORY_USECASE_CLASS;

import android.util.Log;

import com.example.kananizate.feature.data.UserHistoryList;
import com.example.kananizate.feature.domain.repository.UserHistoryListRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserHistoryListUseCase {

    public interface UserHistoryCallBack{
        void onSuccess(UserHistoryList list);
        void onFail(Throwable error);
    }

    public static void start(final UserHistoryCallBack historyCallBack){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        UserHistoryListRepository repository = retrofit.create(UserHistoryListRepository.class);

        Call<UserHistoryList> call = repository.getUserHistoryList();

        call.enqueue(new Callback<UserHistoryList>() {
            @Override
            public void onResponse(Call<UserHistoryList> call, Response<UserHistoryList> response) {
                if(response.isSuccessful()){
                    Log.e(USER_HISTORY_USECASE_CLASS, "Success: "+ response.body().getUser_history());
                    UserHistoryList list = response.body();

                    if(historyCallBack != null){
                        historyCallBack.onSuccess(list);
                    }
                }else{
                    Log.e(USER_HISTORY_USECASE_CLASS, "error: "+response.errorBody());
                    return;
                }
            }

            @Override
            public void onFailure(Call<UserHistoryList> call, Throwable t) {
                Log.e(USER_HISTORY_USECASE_CLASS, "error: "+t.getMessage());
                if(historyCallBack != null){
                    historyCallBack.onFail(t);
                }
            }
        });
    }

}

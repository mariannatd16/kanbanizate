package com.example.kananizate.feature.data;

import android.os.Parcel;
import android.os.Parcelable;

public class EpicListItem implements Parcelable {
    private int id;
    private String epic_name, epic_author;
    private boolean is_remote;

    public EpicListItem() {}

    protected EpicListItem(Parcel in) {
        id = in.readInt();
        epic_name = in.readString();
        epic_author = in.readString();
        is_remote = in.readByte() != 0;
    }

    public static final Creator<EpicListItem> CREATOR = new Creator<EpicListItem>() {
        @Override
        public EpicListItem createFromParcel(Parcel in) {
            return new EpicListItem(in);
        }

        @Override
        public EpicListItem[] newArray(int size) {
            return new EpicListItem[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEpic_name() {
        return epic_name;
    }

    public void setEpic_name(String epic_name) {
        this.epic_name = epic_name;
    }

    public String getEpic_author() {
        return epic_author;
    }

    public void setEpic_author(String epic_author) {
        this.epic_author = epic_author;
    }

    public boolean isIs_remote() {
        return is_remote;
    }

    public void setIs_remote(boolean is_remote) {
        this.is_remote = is_remote;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(epic_name);
        parcel.writeString(epic_author);
        parcel.writeByte((byte) (is_remote ? 1 : 0));
    }
}
